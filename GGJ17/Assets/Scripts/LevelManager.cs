﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class LevelConfig {
    public int camNo;
}

public class LevelManager : MonoBehaviour {

    public EnemyManager EnemyManager;
    public PlayerControl PlayerController {
        get {
            return PlayerManager.currentPlayer;
        }
    }
    public PlayerManager PlayerManager;
    public MainMenu MainMenu;
    public GameOver GameOver;
    public ScreenFader fadeControl;

    public LevelConfig [] levelConfigs;

    private float gameStartTime = 5f;

    void Start() {
        MainMenu.Init ( this );
        GameOver.Init ( this );
        PlayerManager.Init ( this );
        EnemyManager.Init ( this );
    }

    public void StartLevel(int n) {
        var config = levelConfigs [ n ];
        EnemyManager.StartSpawning ();
        PlayerManager.ActivatePlayer ( config.camNo );
    }

    public void ResetGame(System.Action cb) {
        var seq = DOTween.Sequence ();
        seq.AppendCallback ( ( ) => {
            fadeControl.fadeIn = false;
        } )
        .AppendInterval ( 3 )
        .AppendCallback ( ( ) => {
            cb ();
            PlayerManager.ResetAllPlayers ();
            PlayerManager.ActivatePlayer ( 0 );
            MainMenu.Reset ();
            fadeControl.fadeIn = true;
        } );
    }

    //public void ShowBlackScreen(){
    //    GameOverScreen.gameObject.SetActive (true);
    //}

    //public void HideBlackScreen(){
    //    GameOverScreen.gameObject.SetActive (false);
    //}

    //public void OnPlayButtonSelected(){
    //    StartCoroutine (ChangeScene ());

    //}

    //IEnumerator ChangeScene()
    //{
    //    yield return new WaitForSeconds(gameStartTime);
    //    ShowBlackScreen ();
    //    EnemyManager.Init ( this );
    //    PlayerController.Init (this);
    //    HideBlackScreen ();

    //}
}
