﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class PlayerManager : MonoBehaviour {

    public Vector3 [] playerPositions;
    public PlayerControl player;
    public GameObject soundWave;

    public PlayerControl currentPlayer {
        get {
            return player ;
        }
    }

    int currentIndex;

    public SpeechRecognition speechControl;

    public void Init(LevelManager m) {
        player.Init ( m );
        speechControl.Init ();
    }

    public void ActivatePlayer(int num) {
        if(num < playerPositions.Length) {
            player.transform.position = playerPositions [ num ];
            currentIndex = num;
        }
    }

    public void ResetAllPlayers() {
        player.CameraPicker.Head.GetComponent<VignetteAndChromaticAberration> ().enabled = false;
    }
}
