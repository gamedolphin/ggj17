﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public VoiceGun GunControl;
    public CameraPicker CameraPicker;

    public string [] wordsToFire;

    private LevelManager levelManager;

    public Vector3 originalPosition;

    bool initialized = false;

    Coroutine waveCoroutine;

    void Awake() {
    }

    void OnEnable() {
        if(initialized)
            levelManager.PlayerManager.speechControl.OnNewText += OnSpeech;
    }

    void OnDisable() {
        levelManager.PlayerManager.speechControl.OnNewText -= OnSpeech;
    }

    public void Init(LevelManager m ) {
        levelManager = m;
        if(!initialized) {
            levelManager.PlayerManager.speechControl.OnNewText += OnSpeech;
            levelManager.PlayerManager.speechControl.OnMicSound += OnSound;
            initialized = true;
        }
    }

    void OnSound() {
        GunControl.ShowWave ( true );
        if(waveCoroutine != null) {
            StopCoroutine ( waveCoroutine );
        }
        waveCoroutine = StartCoroutine ( HideWave () );
    }

    IEnumerator HideWave() {
        yield return new WaitForSeconds ( 3 );
        GunControl.ShowWave ( false );
    }


    void OnSpeech ( string [] s ) {
        string fireWord = null;
        for ( int i = 0; i < wordsToFire.Length; i++ ) {
            var word = wordsToFire [ i ];
            for ( int j = 0; j < s.Length; j++ ) {
                if ( word == s [ j ].ToUpper () ) {
                    fireWord = word;
                    break;
                }
            }
            if ( fireWord != null ) {
                break;
            }
        }
        if ( fireWord != null ) {
            GunControl.Fire ( fireWord );
        }
    }

    void OnCollisionEnter ( Collision m ) {
        if ( m.collider.tag == "Enemy" ) {
            levelManager.GameOver.StartGameOver ();
        }
    }
}
