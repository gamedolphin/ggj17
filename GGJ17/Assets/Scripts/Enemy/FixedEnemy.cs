﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class FixedEnemy : Enemy {

    [SerializeField]
    private MinMaxRange spawnAngle;

    [SerializeField]
    private MinMaxRange spawnDistance;

    [SerializeField]
    private MinMaxRange waitTime;

    [SerializeField]
    private List<AudioClip> spawnSounds = new List<AudioClip> ();

    [SerializeField]
    private List<AudioClip> dyingSounds = new List<AudioClip> ();

    private Transform playerTransform;


    public override void Init ( EnemyManager m ) {
        base.Init ( m );
        playerTransform = enemyManager.LevelManager.PlayerController.CameraPicker.Head;
        var angle = Quaternion.Euler ( 0 , spawnAngle.value , 0 );
        var forward = Vector3.forward * spawnDistance.value;
        transform.position = playerTransform.position + ( angle * forward );
        playAudio (spawnSounds [Random.Range (0, spawnSounds.Count)]);
    }

    public override void StartMoving ( ) {
        base.StartMoving ();
        var sequence = DOTween.Sequence ();

        sequence.AppendInterval ( waitTime.value ).OnComplete ( ( ) => {
            Destroy ( gameObject );
        });

        tweens.Add ( sequence );
    }

    public override void GettingDestroyed ( ) {
        AudioSource.PlayClipAtPoint (dyingSounds [Random.Range (0, dyingSounds.Count)], this.gameObject.transform.position);

        
    }
}
