﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class EnemyMovement : MonoBehaviour {

    //private Transform target = Camera.main.transform;
    public float speed;
    public float timeToLive = 10f;

	// Use this for initialization
	void Start () {
        transform.DOMove(new Vector3 (0, 1, 1),speed);
        Destroy (gameObject, 10f);
	}
	
}
