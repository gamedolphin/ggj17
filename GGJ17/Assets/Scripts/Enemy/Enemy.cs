﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


[RequireComponent(typeof(AudioSource))]
public class Enemy : MonoBehaviour {

    public enum SpawnType { RANDOM, FIXEDPOSITIONS, INCOGNITO };

    [System.Serializable]
    public class EnemyProperties {
        public float spawnTime;
        public SpawnType enemySpawnStyle;
    }

    protected EnemyManager enemyManager;
    public AudioSource audioSource;
    public float lowPitchRange = .65f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.25f;            //The highest a sound effect will be randomly pitched.

    [SerializeField]
    protected MinMaxRange speed;

    protected List<Sequence> tweens = new List<Sequence> ();

    public virtual void Init(EnemyManager m) {
        enemyManager = m;
    }

    public virtual void StartMoving ( ) {

    }

    public void playAudio(AudioClip sound){
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);
        audioSource.pitch = randomPitch;
        audioSource.spatialBlend = 1.0f;
        audioSource.clip = sound;
        audioSource.Play();

    }

    void OnCollisionEnter(Collision m) {
        if(m.collider.tag == "Bullet") {
            OnBulletCollision ();
        }

    }


    void Awake(){
        audioSource = GetComponent<AudioSource> ();
    }

    void OnDestroy() {
        
        for ( int i = 0; i < tweens.Count; i++ ) {
            tweens [ i ].Kill ();
        }
        enemyManager.SpawnedEnemies.Remove ( this );
    }

    public virtual void GettingDestroyed ( ) {
        
    }

    public virtual void OnBulletCollision() {
        GettingDestroyed();
        Destroy ( gameObject );

    }
}
