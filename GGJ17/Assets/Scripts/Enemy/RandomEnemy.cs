﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class RandomEnemy : Enemy {

    [SerializeField]
    private MinMaxRange spawnAngle;

    [SerializeField]
    private MinMaxRange spawnDistance;

    [SerializeField]
    private MinMaxRange waitTime;

    [SerializeField]
    private List<AudioClip> spawnSounds = new List<AudioClip> ();

    [SerializeField]
    private List<AudioClip> dyingSounds = new List<AudioClip> ();

    private Transform playerTransform;


    public override void Init ( EnemyManager m ) {
        base.Init ( m );
        playerTransform = enemyManager.LevelManager.PlayerController.CameraPicker.Head ;
        var angle = Quaternion.Euler ( 0 , spawnAngle.value , 0 );
        var forward = Vector3.forward * spawnDistance.value;
        transform.position = playerTransform.position + (angle*forward);
        playAudio (spawnSounds [Random.Range (0, spawnSounds.Count)]);
    }

    public override void StartMoving ( ) {
        base.StartMoving ();
        float distance = Vector3.Distance ( transform.position , playerTransform.position );
        var seq = DOTween.Sequence ();
        var t = transform.DOMove ( playerTransform.position , speed.value )
            .SetDelay ( waitTime.value )
            .OnComplete ( ( ) => {
                Destroy ( gameObject );
            } );
        seq.Append ( t );

        tweens.Add ( seq );
    }

    public override void GettingDestroyed ( ) {
        AudioSource.PlayClipAtPoint (dyingSounds [Random.Range (0, dyingSounds.Count)], this.gameObject.transform.position);

    }

}
