﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {

    [System.Serializable]
    public class EnemyListConfig {
        public string EnemyType;
        public MinMaxRange RepeatTime;
        public List<Enemy> List;
    }

    public List<EnemyListConfig> EnemyList = new List<EnemyListConfig>();

    public List<Enemy> SpawnedEnemies = new List<Enemy> ();




    public LevelManager LevelManager;

    //public limitsForRandomSpawn randomSpawnLimits;
    //public Transform[] spawnPoints;
    //public EnemyProperties[] enemies;  // The prefab to be spawned.
    ////private Vector3 spawnPosition;
    //private int length = 10;

    public void Init (LevelManager l) {
        LevelManager = l;
    }

    List<Coroutine> spawnRoutine;

    public void StartSpawning() {
        spawnRoutine = new List<Coroutine> ();
        foreach ( var item in EnemyList ) {
            spawnRoutine.Add(StartCoroutine ( SpawnEnemies ( item ) ));
        }
    }

    public void StopSpawning() {
        for ( int i = 0; i < spawnRoutine.Count; i++ ) {
            StopCoroutine ( spawnRoutine[i] );
        }
        for ( int i = 0; i < SpawnedEnemies.Count; i++ ) {
            Destroy ( SpawnedEnemies [ i ] );
        }
        spawnRoutine.Clear ();
    }

    IEnumerator SpawnEnemies(EnemyListConfig config) {
        while(true) {
            var random = config.List [ Random.Range ( 0 , config.List.Count ) ];
            var enemy = (Instantiate ( random.gameObject ) as GameObject).GetComponent<Enemy>();
            enemy.transform.SetParent ( transform );
            enemy.Init ( this );
            SpawnedEnemies.Add ( enemy );
            enemy.StartMoving ();
            yield return new WaitForSeconds ( config.RepeatTime.value );
        }
    }

    //void Start ( ) {

    //    foreach ( EnemyProperties enemy in enemies ) {
    //        if ( enemy.enemySpawnStyle == spawnType.random ) {
    //            StartCoroutine ( SpawnRandom ( enemy.enemy , enemy.numberOfEnemies , enemy.spawnTime ) );
    //        }
    //        else if ( enemy.enemySpawnStyle == spawnType.fixedPositions ) {
    //            StartCoroutine ( SpawnFixedPosition ( enemy.enemy , enemy.numberOfEnemies , enemy.spawnTime ) );
    //        }
    //    }

    //}

    //public IEnumerator SpawnRandom ( GameObject enemy , int numberOfSpawns , float repeatTime ) {

    //    int i = 0;
    //    while ( i < numberOfSpawns ) {
    //        Vector3 spawnPosition = new Vector3 ( Random.Range ( -randomSpawnLimits.xRangeRandom , randomSpawnLimits.xRangeRandom ) , randomSpawnLimits.yRange , Random.Range ( -randomSpawnLimits.zRangeRandom , randomSpawnLimits.zRangeRandom ) );
    //        Instantiate ( enemy , spawnPosition , Quaternion.identity );
    //        yield return new WaitForSeconds ( repeatTime );
    //    }
    //}

    //public IEnumerator SpawnFixedPosition ( GameObject enemy , int numberOfSpawns , float repeatTime ) {
    //    int i = 0;
    //    while ( i < numberOfSpawns ) {
    //        int spawnPointIndex = Random.Range ( 0 , spawnPoints.Length );
    //        Instantiate ( enemy , spawnPoints [ spawnPointIndex ].position , Quaternion.identity );
    //        yield return new WaitForSeconds ( repeatTime );
    //    }

    //}
}
