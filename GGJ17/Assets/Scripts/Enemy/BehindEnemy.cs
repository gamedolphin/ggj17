﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BehindEnemy : Enemy {

    [SerializeField]
    private MinMaxRange spawnDistance;

    [SerializeField]
    private MinMaxRange waitTime;

    [SerializeField]
    private List<AudioClip> spawnSounds = new List<AudioClip> ();

    [SerializeField]
    private List<AudioClip> dyingSounds = new List<AudioClip> ();

    private Transform playerTransform;


    public override void Init ( EnemyManager m ) {
        base.Init ( m );
        playerTransform = enemyManager.LevelManager.PlayerController.CameraPicker.Head;
        var forward = -playerTransform.forward;
        transform.position = playerTransform.position + ( forward * spawnDistance.value );
        playAudio (spawnSounds [Random.Range (0, spawnSounds.Count)]);
    }

    public override void StartMoving ( ) {
        base.StartMoving ();
        var sequence = DOTween.Sequence ();

        sequence.AppendInterval ( waitTime.value ).OnComplete ( ( ) => {
            Destroy ( gameObject );
        } );

        tweens.Add ( sequence );
    }

    public override void GettingDestroyed ( ) {
        AudioSource.PlayClipAtPoint (dyingSounds [Random.Range (0, dyingSounds.Count)], this.gameObject.transform.position);

        
    }

}
