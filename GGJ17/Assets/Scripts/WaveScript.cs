﻿using UnityEngine;
using System.Collections;

public class WaveScript : MonoBehaviour {

    public float scale = 10.0f;
    public float speed = 1.0f;
    private Vector3 [] baseHeight;
    Mesh mesh;

    void Awake() {
        mesh = GetComponent<MeshFilter> ().mesh;
    }
 
    void Update ( ) {
        if ( baseHeight == null )
            baseHeight = mesh.vertices;

        var vertices = new Vector3 [ baseHeight.Length ];
        for ( var i = 0; i < vertices.Length; i++ ) {
            var vertex = baseHeight [ i ];
            vertex.z += Mathf.Sin ( Time.time * speed +  baseHeight [ i ].y + baseHeight [ i ].z ) * scale;
            vertices [ i ] = vertex;
        }
        mesh.vertices = vertices;
        mesh.RecalculateNormals ();
    }
}
