﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityStandardAssets.ImageEffects;

public class GameOver : MonoBehaviour {

    LevelManager levelManager;

    public GameObject playerBody;
    public GameObject gameOverText;

    public void Init(LevelManager m) {
        levelManager = m;
        gameOverText.SetActive ( false );
    }

    public void StartGameOver() {
        var player = levelManager.PlayerController;
        player.CameraPicker.Head.GetComponent<VignetteAndChromaticAberration> ().enabled = true;
        transform.position = player.transform.position;
        gameOverText.transform.SetParent ( player.transform );
        gameOverText.SetActive ( true );
        playerBody = ( Instantiate ( playerBody ) as GameObject );
        playerBody.transform.position = player.transform.position;
        player.transform.DOMoveY ( player.transform.position.y + 5 , 2);

        var seq = DOTween.Sequence ();
        seq.AppendInterval ( 4 ).AppendCallback ( ( ) => {
            levelManager.PlayerManager.speechControl.OnMicSound += RetryGame;
        } );
        levelManager.EnemyManager.StopSpawning ();
    }

    void RetryGame() {
        var player = levelManager.PlayerController;
        levelManager.ResetGame (()=> {
            gameOverText.SetActive ( false );
            gameOverText.transform.SetParent ( transform );
            gameOverText.transform.localPosition = new Vector3 ( 0 , 0 , 4.69f );
        } );
        levelManager.PlayerManager.speechControl.OnMicSound -= RetryGame;
    }
}
