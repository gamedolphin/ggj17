﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    private LevelManager levelManager;

    public Animator mainMenuAnimator;

    public void Init ( LevelManager m) {
        levelManager = m;
        levelManager.PlayerManager.speechControl.OnMicSound += OnStart;
    }

    void OnStart() {
        mainMenuAnimator.SetTrigger ( "Start" );
        levelManager.StartLevel ( 1 );
        levelManager.PlayerManager.speechControl.OnMicSound -= OnStart;
    }

    public void Reset ( ) {
        levelManager.PlayerManager.speechControl.OnMicSound += OnStart;
        mainMenuAnimator.Play ( "Playbutton" );
    }
}
