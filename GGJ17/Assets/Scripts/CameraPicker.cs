﻿using UnityEngine;
using System.Collections;


public class CameraPicker : MonoBehaviour {

    public bool UseGearVR;

    [SerializeField]
    private GameObject cardboardVR;

    [SerializeField]
    private GameObject gearVR;

    [HideInInspector]
    public Transform Head {
        get {
            if ( UseGearVR ) {
                return gearHead.transform;
            }
            else {
                return cardboardHead.transform;
            }
        }
    }

    [SerializeField]
    private GameObject cardboardHead;

    [SerializeField]
    private GameObject gearHead;

	void Awake () {
        if (UseGearVR == true) {
            if(gearVR != null) {
                gearVR.SetActive ( true );
            }
            if(cardboardVR != null) {
                cardboardVR.SetActive ( false );
            }
        }
        else {
            if ( gearVR != null ) {
                gearVR.SetActive ( false );
            }
            if ( cardboardVR != null ) {
                cardboardVR.SetActive ( true );
            }

        }
    }
}
