﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class VoiceGun : MonoBehaviour {

    [SerializeField]
    private float fireRate;

    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private GameObject soundWave;

    private bool canFire;

    public Transform TrackingHead;

    void Awake() {
        canFire = true;
    }

    IEnumerator ResetFire() {
        yield return new WaitForSeconds ( fireRate );
        canFire = true;
    }

    public void ShowWave(bool show, float intensity = 1) {
        if(show) {
            soundWave.SetActive ( true );
        }
        else {
            soundWave.SetActive ( false );
        }
    }
    
    public void Fire(string s) {
        if(canFire) {
            var b = (Instantiate ( bullet ) as GameObject).GetComponent<Bullet>();
            b.transform.SetParent ( transform );
            b.transform.localPosition = Vector3.zero;
            var movePosition = TrackingHead.forward;
            b.Fire ( movePosition , s);
            canFire = false;
            StartCoroutine ( ResetFire () );
        }
    }
}
