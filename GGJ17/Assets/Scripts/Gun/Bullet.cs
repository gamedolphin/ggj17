﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Bullet : MonoBehaviour {

    [SerializeField]
    protected float bulletSpeed;

    void OnCollisionEnter(Collision c) {
        Destroy ( gameObject );
    }

    public virtual void Fire(Vector3 target, string speech = null) {
        var distance = 100;
        Vector3 distanceTarget = target * distance;
        transform.LookAt ( distanceTarget );
        transform.DOMove ( distanceTarget , distance / bulletSpeed ).OnComplete ( ( ) => {
            Destroy ( gameObject );
        } );
    }
}
