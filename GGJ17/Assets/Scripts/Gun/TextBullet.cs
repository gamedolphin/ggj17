﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(TextMesh))]
public class TextBullet : Bullet {

    TextMesh text;

    void Awake() {
        text = GetComponent<TextMesh> ();
    }

    public override void Fire ( Vector3 target , string speech = null ) {
        text.text = speech;
        base.Fire ( target );
    }
}
