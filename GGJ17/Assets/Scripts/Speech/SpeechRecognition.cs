﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.Audio;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class SpeechRecognition : MonoBehaviour {

    public delegate void SpeechEvent ( string[] s );
    public event SpeechEvent OnNewText;

    public delegate void MicEvent ( );
    public event MicEvent OnMicSound;

    public float threshold;

    public float pollTime;

    private string speechString = "BOOM";

    bool isMicReady = false;

    AudioSource src;

    float[] spectrum = new float [ 64 ];

    IEnumerator Start ( ) {
        src = GetComponent<AudioSource> ();
        isMicReady = false;
        yield return new WaitForSeconds ( 0.5f );
        src.clip = Microphone.Start ( null , true , 10 , 256 );
        src.loop = true;
        src.mute = false;
        while ( !( Microphone.GetPosition ( null ) > 0 ) ) {
            yield return null;
        } // Wait until the recording has started
        src.Play ();
        isMicReady = true;
    }

    public void Init() {

#if UNITY_ANDROID
        AndroidJavaClass jc = new AndroidJavaClass ( "com.unity3d.player.UnityPlayer" );
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ( "currentActivity" );
        var pc = new AndroidJavaClass ( "edu.cmu.pocketsphinx.demo.PocketSphinxActivity" );
        pc.CallStatic ( "runRecognizerSetup" , jo );
        pc.Dispose ();
        jo.Dispose ();
        jc.Dispose ();
        StartCoroutine(GetSpeech());
        Debug.Log ( "HERE" );
#endif

    }

    IEnumerator GetSpeech() {
        while(true) {
#if UNITY_EDITOR
#elif UNITY_ANDROID
            using(var pc = new AndroidJavaClass ( "edu.cmu.pocketsphinx.demo.PocketSphinxActivity" ) ) {
                var str = pc.CallStatic<string> ( "GetCurrentText" );
                Debug.Log ( "STRING FROM JAVA IS " + str );
                ProcessSpeech ( str );
            }
            yield return new WaitForSeconds ( pollTime );
#endif
        }
    }

    void Update() {
        float averageValue = 0;
        src.GetSpectrumData ( spectrum , 0 , FFTWindow.Rectangular );


        for ( int i = 0; i < spectrum.Length - 1; i++ ) {
            averageValue = averageValue + spectrum [ i ];
        }

        averageValue = averageValue / spectrum.Length;
        averageValue = Mathf.Sqrt ( averageValue );
        averageValue = 20 * Mathf.Log10 ( averageValue / 0.1f );

        if(averageValue > threshold) {
            if(OnMicSound != null) {
                OnMicSound ();
            }
        }
    }

    void ProcessSpeech(string s) {
        if(s == null) {
            return;
        }
        string diff = null;
        if ( s.StartsWith ( speechString ) ) {
            diff = s.Substring ( speechString.Length );
            speechString = s;
        }
        else {
            diff = s;
            speechString = s;
        }
        Debug.Log ( diff );
        if ( diff != null ) {
            if ( OnNewText != null )
                OnNewText ( diff.Split(' ') );
        }
    }
    
    void OnDestroy() {
        StopAllCoroutines ();
#if UNITY_EDITOR
#elif UNITY_ANDROID
        using ( var pc = new AndroidJavaClass ( "edu.cmu.pocketsphinx.demo.PocketSphinxActivity" ) ) {
            if ( pc != null ) {
                pc.CallStatic ( "StopListening" );
            }
        }
#endif
    }
}
