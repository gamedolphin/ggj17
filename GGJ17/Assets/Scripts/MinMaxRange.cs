﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MinMaxRange  {

    public float min;
    public float max;

    public float value {
        get {
            return Random.Range ( min , max );
        }
    }
}
